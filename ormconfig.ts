import { ConnectionOptions } from "typeorm";
import { TodoEntity } from "./src/database/entities/TodoEntity";
import { InitialMigration1629577356021 } from "./src/database/migrations/1629577356021-InitialMigration";
import { FixDefaultValueOnCompletedColumn1629578607624 } from "./src/database/migrations/1629578607624-FixDefaultValueOnCompletedColumn";

const databaseConfig: ConnectionOptions = {
  type: "postgres",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT) || 5432,
  database: process.env.DB_DATABASE,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  logging: "all",
  synchronize: false,
  entities: [TodoEntity],
  migrations: [
    InitialMigration1629577356021,
    FixDefaultValueOnCompletedColumn1629578607624,
  ],
  cli: {
    entitiesDir: "src/database/entities",
    migrationsDir: "src/database/migrations",
  },
};

export default databaseConfig;
