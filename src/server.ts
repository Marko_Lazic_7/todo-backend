import { config } from "dotenv";
config();
import "reflect-metadata";
import cors from "cors";

import express from "express";
import corsConfig from "./config/corsConfig";
import { ApolloServer } from "apollo-server-express";
import typeDefs from "./schema/schema";
import resolvers from "./resolvers/resolvers";
import connectToDatabase from "./config/databaseConfig";

const main = async (): Promise<void> => {
  const app = express();

  await connectToDatabase();

  app.use(cors(corsConfig));

  const graphqlServer = new ApolloServer({
    typeDefs: typeDefs,
    resolvers: resolvers,
  });

  await graphqlServer.start();

  graphqlServer.applyMiddleware({ app, path: "/graphql" });

  app.listen(process.env.PORT, () => {
    console.log(
      "App listening on",
      process.env.PORT,
      graphqlServer.graphqlPath
    );
  });
};

main();
