import { createConnection } from "typeorm";

const connectToDatabase = async () => {
  await createConnection()
    .then((connection) => {
      console.log("Succesfully connected to database!");
      return connection;
    })
    .catch((err) => {
      console.log("Unable to connect to database", err);
      throw new Error("UNABLE_TO_CONNECT_TO_DATABASE");
    });
};

export default connectToDatabase;
