import {MigrationInterface, QueryRunner} from "typeorm";

export class FixDefaultValueOnCompletedColumn1629578607624 implements MigrationInterface {
    name = 'FixDefaultValueOnCompletedColumn1629578607624'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."todo" ALTER COLUMN "content" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."todo" ALTER COLUMN "completed" SET DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."todo" ALTER COLUMN "completed" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."todo" ALTER COLUMN "content" SET NOT NULL`);
    }

}
