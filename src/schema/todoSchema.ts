import { gql } from "apollo-server-express";
import { GraphQLID } from "../utils/tsTypes";

export interface ITodo {
  id: GraphQLID;
  title: String;
  content?: String;
  completed: Boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface ICreateTodoInput {
  input: {
    title: String;
    content?: String;
  };
}

export interface IUpdateTodoInput {
  input: {
    id: GraphQLID;
    title: String;
    content?: String;
    completed: boolean;
  };
}

export interface IGetTodoInput {
  filter: {
    id: GraphQLID;
  };
}

const todoSchema = gql`
  type Todo {
    id: ID!
    title: String!
    content: String
    completed: Boolean!
    createdAt: Date!
    updatedAt: Date!
  }

  input GetTodoInput {
    id: ID!
  }

  type Query {
    getTodos: [Todo!]
    getTodo(filter: GetTodoInput!): Todo
  }

  input CreateTodoInput {
    title: String!
    content: String
  }

  input UpdateTodoInput {
    id: ID!
    title: String
    content: String
    completed: Boolean
  }

  type Mutation {
    createTodo(input: CreateTodoInput): Todo!
    updateTodo(input: UpdateTodoInput): Todo!
  }
`;

export default todoSchema;
