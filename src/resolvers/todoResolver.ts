import { getRepository } from "typeorm";
import { TodoEntity } from "../database/entities/TodoEntity";
import {
  ICreateTodoInput,
  IGetTodoInput,
  ITodo,
  IUpdateTodoInput,
} from "../schema/todoSchema";

const todoResolvers = {
  Query: {
    getTodos: async () => {
      const allTodos = await getRepository(TodoEntity).find();
      return allTodos;
    },
    getTodo: async (_: unknown, args: IGetTodoInput) => {
      const todo = await getRepository(TodoEntity).findOneOrFail({
        where: { id: args.filter.id },
      });

      return todo;
    },
  },
  Mutation: {
    createTodo: async (_: unknown, args: ICreateTodoInput) => {
      const newTodo = new TodoEntity();

      newTodo.title = args.input.title;
      newTodo.content = args.input.content;

      const savedTodo = await newTodo.save();

      return savedTodo;
    },
  },
  updateTodo: async (_: unknown, args: IUpdateTodoInput) => {
    if (
      !args.input.title &&
      !args.input.content &&
      typeof args.input.completed !== "boolean"
    ) {
      throw new Error("You must specific at least one property to update");
    }

    const todo = await getRepository(TodoEntity).findOneOrFail({
      where: { id: args.input.id },
    });

    if (args.input.title) {
      todo.title = args.input.title;
    }

    if (args.input.content) {
      todo.content = args.input.content;
    }

    if (typeof args.input.completed === "boolean") {
      todo.completed = args.input.completed;
    }

    const updatedTodo = await todo.save();

    return updatedTodo;
  },
};

export default todoResolvers;
